Gem::Specification.new do |s|
    s.name        = 'content_storable'
    s.version     = '0.0.0'
    s.licenses    = ['BSD-3-Clause']
    s.summary     = 'Simple content-addressable store behaviour'
    s.description = <<-DESC
    DESC
    s.authors     = ['ADHOC-GTI']
    s.email       = 'foss@adhoc-gti.com'
    s.files       = Dir['lib/**/*.rb'] + ['LICENSE']
    s.homepage    = 'https://github.com/adhoc-gti/content_storable'
  end